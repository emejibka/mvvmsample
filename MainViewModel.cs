﻿using System;
using System.Windows.Input;

namespace mvvmSample
{
    public class MainViewModel
    {
        public string X { get; set; }
        public string Y { get; set; }

        private ICommand _plusCommand = new PlusCommand();
        public ICommand PlusCommand { get { return _plusCommand; } }
    }

    public class PlusCommand : ICommand
    {
        public void Execute(object parameter)
        {
        }

        public bool CanExecute(object parameter)
        {
            var mainViewModel = parameter as MainViewModel;

            if (mainViewModel == null)
                return true;

            int parseResult;
            return string.IsNullOrEmpty(mainViewModel.X) == false
                   && string.IsNullOrEmpty(mainViewModel.Y) == false
                   && int.TryParse(mainViewModel.X, out parseResult)
                   && int.TryParse(mainViewModel.Y, out parseResult);
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
}